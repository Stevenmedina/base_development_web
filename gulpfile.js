"use strict";

var gulp        = require('gulp'),
    gulpif      = require('gulp-if'),
    jade        = require('gulp-jade'),
    sass        = require('gulp-sass'),
    csso        = require('gulp-csso'),
    coffee      = require('gulp-coffee'),
    uglify      = require('gulp-uglify'),
    concat      = require('gulp-concat'),
    sourcemaps  = require('gulp-sourcemaps'),
    browserSync = require('browser-sync').create();

var paths = {
	app: './app',
	dest: './dist'
};

gulp.task('server', function() {
    browserSync.init({
        server: "./dist",
        port: 9000
    });
});

gulp.task('template', function() {
  return gulp.src([paths.app + '/**/*.jade', '!app/base/**/*.jade'])
    .pipe(jade({
      pretty: true
    }))
    .pipe(gulp.dest(paths.dest));
});

gulp.task('css', function() {
  return gulp.src(paths.app + '/assets/sass/*.scss')
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        includePaths: [paths.app + '/assets/sass/'],
        errLogToConsole: true
      }))
    .pipe(csso())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest(paths.dest + '/css/'));
});

gulp.task('js', function() {
  return gulp.src([paths.app + '/assets/coffee/*.coffee'])
    .pipe(sourcemaps.init())
    .pipe(gulpif(/[.]coffee$/, coffee()))
    .pipe(uglify())
    .pipe(concat('all.min.js'))
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest(paths.dest + '/js/'));
});

gulp.task('vendor-css', function() {
  return gulp.src([paths.app + '/assets/css/*.css', paths.app + '/assets/css/*.map'])
    .pipe(gulp.dest(paths.dest + '/css/vendor/'));
});

gulp.task('vendor-js', function() {
  return gulp.src([paths.app + '/assets/js/*.js'])
    .pipe(gulp.dest(paths.dest + '/js/vendor/'));
});

gulp.task('vendor-php', function() {
  return gulp.src([paths.app + '/assets/php/*.php'])
    .pipe(gulp.dest(paths.dest + '/'));
});

gulp.task('images', function() {
  return gulp.src([paths.app + '/assets/img/**/*'])
    .pipe(gulp.dest(paths.dest + '/img/'));
});

gulp.task('watch', function () {
  gulp.watch(paths.app + '/assets/sass/**/*.scss',['css']);
  gulp.watch(paths.app + '/assets/coffee/**/*.coffee',['js']);
  gulp.watch(paths.app + '/**/*.jade',['template']);
  gulp.watch(paths.app + '/assets/img/**/*.*',['images']);
});

gulp.task('default', ['js','css','template', 'watch', 'vendor-css', 'server', 'vendor-js', 'vendor-php', 'images']);
gulp.task('build', ['js','css','template', 'vendor-css', 'vendor-js', 'vendor-php', 'images']);